import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import request from 'supertest';
import { AppModule } from '../src/app.module';
import { ALL_SHIPS } from './data/ship.data';
import { getShipCommand } from './factories/ship.factory';
import { createConnection, getConnectionOptions } from 'typeorm';
import { Ship } from '../src/ship/ship.entity';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let connection;

  beforeAll(async () => {
    const options = {
      ...(await getConnectionOptions()),
      entities: [Ship],
    };
    connection = await createConnection(options);

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    // TODO it would be better to use fixtures to seed data and set DB to same state before each running of tests
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(HttpStatus.OK)
      .expect('Shipyard api');
  });

  it('/ships(GET) get all ships - SUCCESS', async () => {
    const response = await request(app.getHttpServer()).get('/ships');

    expect(response.status).toBe(HttpStatus.OK);
    expect(response.body.data).toEqual(ALL_SHIPS);
  });

  it('/ships(POST) create ship - SUCCESS', async () => {
    const response = await request(app.getHttpServer())
      .post('/ships')
      .send(getShipCommand({ name: 'new one' }));

    expect(response.status).toBe(HttpStatus.CREATED);
  });

  [
    [
      'create ship ERROR - BAD REQUEST, name is required',
      getShipCommand(),
      HttpStatus.BAD_REQUEST,
    ],
    [
      'create ship ERROR - BAD REQUEST, type is required',
      getShipCommand({ type: undefined }),
      HttpStatus.BAD_REQUEST,
    ],
    [
      'create ship ERROR - BAD REQUEST, speed is required',
      getShipCommand({ speed: undefined }),
      HttpStatus.BAD_REQUEST,
    ],
    [
      'create ship ERROR - CONFLICT, ship with is name already exists',
      getShipCommand({ name: 'Enterprise' }),
      HttpStatus.CONFLICT,
    ],
  ].forEach(([label, payload, status]) => {
    it(`/ships(POST) - ${label}`, async () => {
      const response = await request(app.getHttpServer())
        .post('/ships')
        .send(payload as object);

      expect(response.status).toBe(status);
    });
  });

  afterAll(async () => {
    const SHIP_NAME = 'new one';

    const shipRepository = connection.getRepository(Ship);
    const ship = await shipRepository.findOne({
      where: {
        name: SHIP_NAME,
      },
    });
    if (ship) {
      await shipRepository.remove(ship);
    }

    await app.close();
    await connection.close();
  });
});
