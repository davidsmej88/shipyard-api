export const ALL_SHIPS = [
  {
    id: '00000000-0000-0000-0000-000000000001',
    created_at: '2022-07-22T11:57:33.297Z',
    updated_at: '2022-07-22T11:57:33.297Z',
    name: 'Enterprise',
    type: 'enterprise',
    speed: 'super fast',
  },
  {
    id: '00000000-0000-0000-0000-000000000002',
    created_at: '2022-07-22T14:00:37.000Z',
    updated_at: '2022-07-22T14:00:38.000Z',
    name: 'Galaxy',
    type: 'galaxy',
    speed: 'super slow',
  },
];
