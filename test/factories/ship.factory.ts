export const getShipCommand = ({
  name = undefined,
  type = 'enterprise',
  speed = 'super fast',
} = {}) => {
  return {
    name,
    type,
    speed,
  };
};
