import { ConflictException, Injectable } from '@nestjs/common';
import { ShipEntityRepository } from './ship.entityRepository';
import { ShipDto, ShipSaveCommand } from './ship.dto';
import { toShipEntity } from './mappers/ship.mapper';

@Injectable()
export class ShipService {
  constructor(private readonly shipRepository: ShipEntityRepository) {}

  getAllShips(): Promise<ShipDto[]> {
    return this.shipRepository.getAll();
  }

  async saveShip(shipSaveCommand: ShipSaveCommand): Promise<ShipDto> {
    let ship = await this.shipRepository.getByName(shipSaveCommand.name);
    if (ship) {
      throw new ConflictException(
        `Ship with name: ${shipSaveCommand.name} already exists.`,
      );
    }

    ship = toShipEntity(shipSaveCommand);
    return this.shipRepository.save(ship);
  }
}
