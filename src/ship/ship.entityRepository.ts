import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Ship } from './ship.entity';

export class ShipEntityRepository {
  constructor(
    @InjectRepository(Ship)
    private readonly shipRepository: Repository<Ship>,
  ) {}

  async getById(id: string): Promise<Ship> {
    return this.shipRepository.findOne({ id });
  }

  async getByName(name: string): Promise<Ship> {
    return this.shipRepository.findOne({
      where: {
        name,
      },
    });
  }

  async getAll(): Promise<Ship[]> {
    return this.shipRepository.find();
  }

  async save(ship: Ship): Promise<Ship> {
    return this.shipRepository.save(ship);
  }
}
