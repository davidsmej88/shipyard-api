import { Module } from '@nestjs/common';
import { ShipController } from './ship.controller';
import { ShipService } from './ship.service';
import { ShipEntityRepository } from './ship.entityRepository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Ship } from './ship.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Ship])],
  controllers: [ShipController],
  providers: [ShipService, ShipEntityRepository],
  exports: [ShipService, ShipEntityRepository, TypeOrmModule],
})
export class ShipModule {}
