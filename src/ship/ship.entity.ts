import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../common/common.entity';

@Entity()
export class Ship extends BaseEntity {
  @Column({ nullable: false, unique: true })
  name: string;

  // TODO it would be better to use enum for ship types
  @Column({ nullable: false })
  type: string;

  @Column({ nullable: false })
  speed: string;
}
