import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ShipDto {
  @IsUUID()
  @ApiProperty({
    description: 'Ship uuid',
  })
  id: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Ship type',
  })
  type: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Ship speed',
  })
  speed: string;
}

export class ShipSaveCommand {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Ship name. Is unique',
  })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Ship type',
  })
  type: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'Ship speed',
  })
  speed: string;
}
