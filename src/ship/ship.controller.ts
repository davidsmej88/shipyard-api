import { Body, Controller, Get, Post } from '@nestjs/common';
import {
  ApiTags,
  ApiBearerAuth,
  ApiExtraModels,
  ApiBadRequestResponse,
  ApiConflictResponse,
} from '@nestjs/swagger';
import { OkManyResponse, OkOneResponse } from '../common/common.apiResponse';
import { ShipService } from './ship.service';
import {
  ApiOkManyResponse,
  ApiOkOneResponse,
} from '../common/common.decorators';
import { ShipDto, ShipSaveCommand } from './ship.dto';

@Controller('ships')
@ApiTags('Ship')
@ApiExtraModels(OkOneResponse, ShipDto, OkManyResponse)
@ApiBearerAuth()
export class ShipController {
  constructor(private readonly shipService: ShipService) {}

  @Get()
  @ApiOkManyResponse(ShipDto)
  async getMany(): Promise<OkManyResponse<ShipDto>> {
    const response = new OkManyResponse<ShipDto>();
    response.data = await this.shipService.getAllShips();

    return response;
  }

  @Post()
  @ApiBadRequestResponse({ description: 'Incorrect parameters.' })
  @ApiConflictResponse({ description: 'Ship with this name already exists' })
  @ApiOkOneResponse(ShipDto)
  async save(
    @Body() shipSaveCommand: ShipSaveCommand,
  ): Promise<OkOneResponse<ShipDto>> {
    const response = new OkOneResponse<ShipDto>();
    response.data = await this.shipService.saveShip(shipSaveCommand);
    return response;
  }
}
