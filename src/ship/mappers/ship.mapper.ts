import { ShipSaveCommand } from '../ship.dto';
import { Ship } from '../ship.entity';

export const toShipEntity = (shipSaveCommand: ShipSaveCommand): Ship => {
  const ship = new Ship();
  ship.type = shipSaveCommand.type;
  ship.speed = shipSaveCommand.speed;
  ship.name = shipSaveCommand.name;

  return ship;
};
