import { ApiProperty } from '@nestjs/swagger';

export class OkOneResponse<TData> {
  constructor(message = 'OK') {
    this.message = message;
  }
  @ApiProperty()
  data: TData;

  @ApiProperty()
  message: string;
}

export class OkManyResponse<TData> {
  constructor(message = 'OK') {
    this.message = message;
  }
  @ApiProperty()
  data: TData[];

  @ApiProperty()
  message: string;
}
