import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as typeorm from 'typeorm';
import { getConnectionOptions } from 'typeorm';
import { Ship } from './ship/ship.entity';
import { ShipModule } from './ship/ship.module';

export const getNestConnectionOptions =
  async (): Promise<typeorm.ConnectionOptions> =>
    Object.assign(await getConnectionOptions(), {
      entities: [Ship],
      keepConnectionAlive: true,
    });

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      useFactory: getNestConnectionOptions,
    }),
    ShipModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
