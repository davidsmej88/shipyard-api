## Description

REST api to manage shipyard

## Local dev setup

```bash
1. copy .env.example to .env
2. $ docker-compose up
3. $ npm install
```

## Running the app

```bash
1. $ npm run start
2. run sql script ./dbInit/ships.sql to seed data to DB
```

## Test

```bash
$ npm run test:e2e
don't forget to run seed script to fill data to DB
```
## Tech stack

- Node.js with NestJS framework
- To store data is used Postgresql dockerized in Docker
- For testing is used Jest